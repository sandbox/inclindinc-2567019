(function ($, Drupal, undefined) {
  Drupal.behaviors.escapia_api = {
    attach: function (context, settings) {

      if (typeof settings.escapia_api != 'undefined') {

        if (typeof settings.escapia_api.dateSettings != 'undefined') {
          $.each(settings.escapia_api.dateSettings, function(index, value) {
            var settings_val = JSON.parse(value);
            settings_val['onClose'] = function(){
              $(document.activeElement).blur();
            }
            $('.' + index).pickadate(settings_val);
          });
        }
      }

      $("#escapia-avail-rate-details").dialog({
        autoOpen: false,
        draggable: false,
        height: 'auto',
        width: 300,
        modal: true,
        resizable: false,
        title: 'Quote Details',
        closeText: 'Close',
        overlay: { background: "black" }
      });

      $( "#escapia-avail-rate-details-link" ).click(function(e) {
        e.preventDefault();
        $( "#escapia-avail-rate-details" ).dialog( "open" );
      });
    }
  };
})(jQuery, Drupal);