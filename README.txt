Instructions on setting up Escapia Feed:

ESCAPIA API documentation for reference: https://customer.escapia.com/distribution/api/evrn-api-documentation


Installation Notes
==================
1. Field Collection module needs to be at least


Configuration
=============

1. After enabling escapia_wsclient_feeds feature, there will be configuration created for Feed importers and for WebService Client modules to provide all the
   basic Escapia API functionality for importing units:
   - admin/config/services/wsclient/manage/escapia_api   - contains all the operations / data types based on ESCAPIA WSDL structure (UnitSearch and UnitDescriptiveInfo operations
     are already pre-configured with correct parameters / data structure. Other operations might require some tweaking as they require authentication and some structural changes because
     automatic WSDL import cannot account for all the use cases like multiple values, etc.)

2. Before the Feeds can be utilized, update Authentication Parameters for all of the Escapia Feed Importers at "admin/config/escapianet/import-settings": username and password for your
   Escapia API account.

3. Escapia API UnitSearch() operation requires at list one Search Criteria to be set. Default configuration for the search is based on ADDRESS parameters, which only has COUNTRY field
   set to "US" value. This configuration will allow to search all of your rental properties within your Escapia Account (of course, if they are located in the USA). Change any
   parameters as necessary.

4. To trigger initial import of Escapia Rental Properties, run UnitsSearch Feed Import from 'import/escapia_api_units_search'.


Additional Features
====================

1. Unit Reviews

Functionality can be added by enabling Escapia API Reviews (escapia_reviews_wsclient_feeds) module.
It will create addition Escapia Unit Review content type and set an entity_reference field on Escapia Unit nodes to tie reviews to units.
"Escapia Rentals (Reviews)" feed importer will be responsible for importing reviews into Drupal. Frequency of Reviews updates can be configured at "admin/config/escapianet/import-settings"

NOTE:  Unit Reviews is relying on "Star Rating" module, which needs to be patched, if it's not yet fixed (7.x-1.1 version) for it's NULL values - find a patch in escapia_api folder.

2. Booking Availability Check

Functionality can be added by utilizing provided "Escapia API: Check Unit Availability Form" block.
This block is calling Escapia API UnitStay function.

3. Unit Reservations

Functionality can be added by enabling "Escapia API (Feeds): Book Online" module (escapia_api_booking).

NOTE:  At this moment it is tested to work up to the point of credit card payment... I am awaiting on credentials to verify complete functionality.
 For credit cards processing with Escapia you will need to set up your billing account with them first, and then obtain HAPI ClientID and APIKey. This information needs to be added to
 configuration settings form at "admin/config/escapianet/import-settings" path.
